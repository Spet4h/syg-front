# syg

## Project setup
```
npm install


Aplicación WEB - Proyecto Productivo - SENA 2022
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
